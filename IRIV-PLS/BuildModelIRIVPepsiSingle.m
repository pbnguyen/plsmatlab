clear all;clf;
load('../data/Pepsi.mat');
XTrain = XTrainPepsi;
yTrain = yTrainPepsi;
PepsiIRIVModel = iriv(XTrain,yTrain,10,5,'center');
save('.\CocaIRiv.mat','CocaIRIVModel','XTrainCoca','yTrainCoca','XTestCoca','yTestCoca');