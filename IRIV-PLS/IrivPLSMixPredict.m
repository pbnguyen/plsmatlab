clear;clf;
load('.\IrivMix.mat');
CocaSelectVar = CocaIRIV_Mix.SelectedVariables;
PepsiSelectVar = PepsiIRIV_Mix.SelectedVariables;

XTrainCoca = XTrainMix(:,CocaSelectVar);
yTrainCoca = yTrainMix(:,1);
XTestCoca  = XTestMix(:,CocaSelectVar);

XTrainPepsi = XTrainMix(:,PepsiSelectVar);
yTrainPepsi = yTrainMix(:,2);
XTestPepsi  = XTestMix(:,PepsiSelectVar);

yTest  = yTestMix;

A = 10;
method = 'center';
PLSMixResultCoca = pls(XTrainCoca,yTrainCoca,A,method);
% y = WX
W1 = PLSMixResultCoca.regcoef_original;
X1 = [XTestCoca ones(size(XTestCoca,1),1)];
y1 = X1*W1;

PLSMixResultPepsi = pls(XTrainPepsi,yTrainPepsi,A,method);
W2 = PLSMixResultPepsi.regcoef_original;
X2 = [XTestPepsi ones(size(XTestPepsi,1),1)];
y2 = X2*W2;

fprintf('\tValue \t\t\t\t Predict\r\n')
fprintf('Coca \t Pepsi \t\t Coca \t Pepsi\r\n');
RMSEP_Coca = 0;
RMSEP_Pepsi = 0;
for i=1:length(y1)
    RMSEP_Coca  =  RMSEP_Coca + (yTest(i,1)-y1(i))^2;
    RMSEP_Pepsi =  RMSEP_Pepsi + (yTest(i,2)-y2(i))^2;
    fprintf('%.2f \t %.2f \t\t %.2f \t %.2f \r\n',yTest(i,1),yTest(i,2),y1(i),y2(i));
end
subplot(1,2,1);
hold on; grid on;
syms x;
ezplot(x,[0 100]);
title('Coca');
xlabel('observed')
ylabel('predicted')
for i=1:length(y1)
    text(yTest(i,1),y1(i),'x','Color','red');
end
subplot(1,2,2);
hold on; grid on;
ezplot(x,[0 100]);
title('Pepsi');
xlabel('observed')
ylabel('predicted')
for i=1:length(y2)
    text(yTest(i,2),y2(i),'x','Color','red');
end
RMSEP_Coca  = sqrt(RMSEP_Coca/length(y1));
RMSEF_Coca = PLSMixResultCoca.RMSEF;
RMSEP_Pepsi  = sqrt(RMSEP_Pepsi/length(y1));
RMSEF_Pepsi = PLSMixResultPepsi.RMSEF;
fprintf('Coca: RMSEF = %f, RMSEP = %f \r\n',RMSEF_Coca,RMSEP_Coca);
fprintf('Pepsi: RMSEF = %f, RMSEP = %f \r\n',RMSEF_Pepsi,RMSEP_Pepsi);

