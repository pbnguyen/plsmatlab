clear all;clf;
load('../data/Mix.mat');
XTrain = XTrainMix;
yTrain = yTrainMix(:,1);
CocaIRIV_Mix = iriv(XTrain,yTrain,10,5,'center');
yTrain = yTrainMix(:,2);
PepsiIRIV_Mix = iriv(XTrain,yTrain,10,5,'center');
save('IrivMix.mat');