clear all;clf;
load('../data/Coca.mat');
XTrain = XTrainCoca;
yTrain = yTrainCoca;
CocaIRIVModel = iriv(XTrain,yTrain,10,5,'center');
save('.\CocaIRiv.mat','CocaIRIVModel','XTrainCoca','yTrainCoca','XTestCoca','yTestCoca');