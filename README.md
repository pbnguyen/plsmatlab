# PLSMatlab

### Thông tin về dữ liệu

Dữ liệu được tách thành 2 phần:

```
    - Phần tạo model PLS (tập train)

    - Phần dữ liệu test (tập test)

```

Mỗi nồng độ có 3 mẫu: 2 mẫu để train và 1 mẫu còn lại để test.

Tổng cộng:

```
    - Tập train: 14 mẫu (mỗi nồng độ 2 mẫu)

    - Tập test: 7 mẫu, mỗi nồng độ 1 mẫu 

```
### Code tạo dữ liệu

Nằm trong thư mục ```data```

1. Dữ liệu đơn chất Coca

    Chạy file ```getCocaData.m```, dữ liệu sẽ lưu vào file ```Coca.mat```

2. Dữ liệu đơn chất Pepsi

    Chạy file ```getPepsi.m```, dữ liệu sẽ lưu vào file ```Coca.mat```
3. Dữ liệu hỗn hợp

    Chạy file ```getMix.m```, dữ liệu sẽ lưu vào file ```Mix.mat```

### Nhận dạng đơn chất
#### Coca đơn chất

Chạy file ```CocaSingle.m``` trong thư mục ```src```.

### Pepsi đơn chất

Chạy file ```PepsiSingle.m``` trong thư mục ```src```.

### Nhận dạng hợp chất

Chạy file ```mixPredict.m``` trong thư mục ```src```.

### Code PLS

``
Không nên đọc code vội, cần coi và hiểu trước 1 số vấn đề phía dưới.
``
#### Cần coi và hiểu trước ngày thứ 6 tuần này

1. Hồi quy tuyến tính là gì ?

2. Hồi quy đa biến tuyến tính là gì ?

3. Bình phương tối thiểu là gì ?

4. Sự khác biệt giữa các phương pháp bình phương tối thiểu ? 

Tập trung vào mô hình toán, ưu điểm, nhược điểm, trường hợp sử dụng.

```
 - Phương pháp bình phương tối thiểu thông thường(classical least square-CLS).

 - Phương pháp bình phương tối thiểu nghịch đảo (inverse least square- ILS).

 - Phương pháp bình phương tối thiểu từng phần (partial least square-PLS ).

 - Phương pháp hồi qui cấu tử chính ( principal component regression -PCR).

```

5. Tại sao phải tiền xử lý dữ liệu trong các bài toán hồi quy tuyến tính ?

6. Các phương pháp tiền xử lý dữ liệu dữ liệu ?

### Tài liệu:

- File "Thuật toán hồi quy đa biến" trong thư mục tài liệu.

- Đọc thật kỹ không sót 1 chữ và hiểu tất cả các công thức trong link:

```

https://machinelearningcoban.com/2016/12/28/linearregression/

```

- Đọc các phần 1,2,3 nắm được các công thức toán về chuẩn hoá dữ liệu (tiền xử lý dữ liệu)

```

https://vimentor.com/vi/lesson/tien-xu-ly-du-lieu-trong-linh-vuc-hoc-may-phan-3

```