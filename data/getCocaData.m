%% Explanation:
% simplified code to read measurement information from an .csv file
% then use the information to calculate intensity features 

%% initial conditions
clear all
FileName={ '..\dungdich\Coca1_6.csv'
           '..\dungdich\Coca2_5.csv'
           '..\dungdich\Coca3_4.csv'
           '..\dungdich\Coca4_3.csv'
           '..\dungdich\Coca5_2.csv'
           '..\dungdich\Coca6_1.csv'
           '..\dungdich\Coca.csv'};
        
y = [
    1/7
    1/7
    1/7
    2/7
    2/7
    2/7
    3/7
    3/7
    3/7
    4/7
    4/7
    4/7
    5/7
    5/7
    5/7
    6/7
    6/7
    6/7
    1
    1
    1
]*100;
% first_run= 0 % set to 1: show plot measurement over time; 0: show estimated features
water_index=[1 3 5]; % indexes of water
solution_index=[2 4 6]; % indexes of solution
test_time=5; % integration time of measurement
wave_range=[400 850]; % define wavelength range
shutter_threshold=200; % intensity threshold to determine shutter on/off
X = [];
for num= 1:size(FileName,1)
    % %% reading .csv data file
    temp = FileName{num,1};
    M=csvread(temp,0,1);

    %% extract wavelength infor and remove data outside selected wavelength range
    wavelength=M(1,:); %the first row is wavelength
    n7=find(wavelength<wave_range(1));
    n8=find(wavelength>wave_range(2));
    wavelength(:,[n7,n8])=[]; % remove wavelength data points outside wavelength range
    M(:,[n7,n8])=[]; % remove intensity columns outside wavelength range
    M(1,:)=[]; % remove wavelength row
    testing=exist( 'measure_correct', 'var'); % create corrected matrix variance size NxM
    if testing==0
        measure_correct=zeros(sum(sum(~isnan(test_time),2)),size(M,2));
    end
    % find wavelength with max intensity, find shutter on/off time
    [~, max_col]=find(M==max(M(:)));
    tem1=M(:,max_col(1)); % take max wavelength in time
    tem2=find(tem1>shutter_threshold); % find indexes with intensity > threshold
    tem3=find(diff(tem2)>1); % find discontinous value of indexes > threshold
    di=zeros(1,size(tem3,1)*2+2); % matrix index of sub-sequences 
    di(1)=tem2(1); % start index of first sequence
    di(size(di,2))=tem2(size(tem2,1));  % stop index of last sequence
    for j=1:size(tem3,1) % stop and start indexes of previous and next sequences
        di(j*2)=tem2(tem3(j)-2);
        di(j*2+1)=tem2(tem3(j)+2);
    end
    tem1=mean(M(di(1):di(2),max_col),1); % remove first di section if shutter off
    if tem1<=shutter_threshold
        di=di(2:end);
    end 
    tem1=mean(M(di(end-1):di(end),max_col),1); % remove last di section if shutter off
    if tem1<=shutter_threshold
        di=di(1:end-1);
    end
    measure_matrix_size=round(length(di)/2); % no. of sequences 
    disp(['No. of measurement sequences:', num2str(measure_matrix_size)]);

    %% extract feature
    k=0;
    for j=1:round(size(di,2)/2)
        k=k+1;
        extract_data=M(di(j*2-1):di(j*2),:);
        integration_ratio=extract_data/test_time;
        for j2=1:size(M,2)
            no_outliner=integration_ratio(:,j2);
            meanValue = mean(no_outliner);
            absoluteDeviation = abs(no_outliner - meanValue);
            mad = median(absoluteDeviation);
            sensitivityFactor = 3;
            thresholdValue = sensitivityFactor * mad;
            outlierIndexes = abs(absoluteDeviation) > thresholdValue;
            no_outliner = no_outliner(~outlierIndexes);
            no_noise=mean(no_outliner);
            measure_correct(k,j2)=no_noise;
        end
    end

    feature=zeros(size(water_index,2),size(measure_correct,2)); 
    for i=1:size(water_index,2)
        feature(i,:)=measure_correct(solution_index(i),:)./measure_correct(water_index(i),:);
    end
    X = [X ; feature];
end
hold on
grid on
XTrainCoca = [];
XTestCoca = [];
yTrainCoca =[];
yTestCoca = [];
for i=1:size(X,1)
    plot(wavelength,X(i,:));
    if ( mod(i,3) == 0)
        XTestCoca = [XTestCoca; X(i,:)];
        yTestCoca = [yTestCoca; y(i)];
    else
        XTrainCoca = [XTrainCoca; X(i,:)];
        yTrainCoca = [yTrainCoca; y(i)];
    end
end;
title('Features');
xlabel('Wavelength (nm)')
ylabel('Intensity ratio')
 save('.\Coca.mat','XTrainCoca','yTrainCoca','XTestCoca','yTestCoca','wavelength');
%save('D:\LUAN VAN TOT NGHIEP\libPLS_1.98\Pepsi','X','y');