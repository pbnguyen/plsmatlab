function CV=vcnsizeplslda(X,y,G,nMax,A,K,method,PROCESS,order)
%+++ K-fold Cross-validation for PLS
%+++ Input:  X: m x n  (Sample matrix)
%            y: m x 1  (measured property)
%            A: The maximal number of latent variables for cross-validation
%            K: fold. when K=m, it is leave-one-out CV
%       method: pretreatment method. Contains: autoscaling,
%               pareto,minmax,center or none.
%      PROCESS: =1 : print process.
%               =0 : don't print process.
%+++ Order: =1  sorted, default. For CV partition.
%           =0  random. 
%+++ Output: Structural data: CV
%+++ Hongdong Li, Oct. 16, 2008.
%+++ Tutor: Prof. Yizeng Liang, yizeng_liang@263.net.
%+++ Contact: lhdcsu@gmail.com.

if nargin<9;order=1;end
if nargin<8;PROCESS=1;end;
if nargin<7;method='center';end;
if nargin<6;K=10;end;
if nargin<5;A=3;end;
if nargin<4;nMax=10;end

uG=unique(G);
uG=-sort(-uG);
uG=uG(1:nMax);
nG=length(uG);

vsel=[];
for i=1:nG
  [rank,b]=find(G>=uG(i));
  if i==1;
    vsel=unique(rank);
  else
    vseltemp=unique(rank);
    vsel=[vsel;setdiff(vseltemp,vsel)];
  end
  Xi=X(:,rank);  
  CVtemp=plsldacv(Xi,y,A,K,method,PROCESS,order);
  cv(i)=CVtemp.minCV;
end

%+++ output
CV.cv=cv;
CV.rank=vsel;



