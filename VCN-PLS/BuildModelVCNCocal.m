clear;
%+++ data import
load('..\data\Coca.mat');
%+++ Compute
X=pretreat(XTrainCoca,'autoscaling');
Y=pretreat(yTrainCoca,'autoscaling');
A=10;
Q=15;
N=20000;
K=5;
%+++ Model population analysis
F=vcn(X,Y,A,'autoscaling',N,Q,K);

%+++ Compute Variable Complementary Network(vcn). The resulting G is then
%    imported to R to draw VCN using the R-package named 'igraph' which is
%    freely available at: http://cran.at.r-project.org/
G=computeNetworkGlobal(F);   %+++ G stores complementary information between variables
save('.\VCNCoca.mat');