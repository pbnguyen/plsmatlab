clear all;clf;

load('../data/Mix.mat');
XTrain = XTrainMix;
yTrain = yTrainMix(:,1);

X=pretreat(XTrain,'autoscaling');
Y=pretreat(yTrain,'autoscaling');
A=5;
Q=15;
N=20000;
K=5;
%+++ Model population analysis
F1=vcn(X,Y,A,'autoscaling',N,Q,K);
G1=computeNetworkGlobal(F1);   %+++ G stores complementary information between variables

yTrain = yTrainMix(:,2);
Y=pretreat(yTrain,'autoscaling');
F2=vcn(X,Y,A,'autoscaling',N,Q,K);
G2=computeNetworkGlobal(F2);   %+++ G stores complementary information between variables
save('VCNMix.mat');