clear;
%+++ data import
load('VCNCoca.mat');
A=10;
figure(1);
bar(sum(G));
xlabel('variable index');ylabel('total complementary information');
figure(2);
imagesc(G);
colorbar;
TempArr = sum(G);
selectVar = [];
value = mean(TempArr);
selectVar = find(TempArr > value);
figure(3);
XTrain = XTrainCoca(:,selectVar);
yTrain = yTrainCoca;
XTest  = XTestCoca(:,selectVar);
yTest  = yTestCoca;
A = 10;
method = 'center';
clf;
PLSCocaResult = pls(XTrain,yTrain,A,method);
title('Features Pretreat');
xlabel('Wavelength (nm)') 
ylabel('Intensity ratio')
% y = WX
W = PLSCocaResult.regcoef_original;
X = [XTest ones(size(XTest,1),1)];
y = X*W;
hold on; grid on;
syms x;
ezplot(x,[0 100]);
title('VCN + PLS Coca Predict');
xlabel('observed')
ylabel('predicted')
fprintf('value \t\t predict \n');
RMSEP = 0;
for i=1:length(y)
    text(yTest(i),y(i),'x','Color','red')
    fprintf('%.2f \t\t %.2f \r\n',yTest(i),y(i));
    RMSEP = RMSEP + (yTest(i)-y(i))^2;
end
RMSEP = sqrt(RMSEP/length(y));
RMSEF = PLSCocaResult.RMSEF;
fprintf('RMSEF = %f \r\n',RMSEF);
fprintf('RMSEP = %f \r\n',RMSEP);


