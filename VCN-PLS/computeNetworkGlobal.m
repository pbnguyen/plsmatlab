function [Z]=computeNetworkGlobal(F,q)
%+++ weightCriterion: 0  (maxError-error) weighted further by cos(bias)
%                     1   maxError-error  
%                     2   maxError/error


if nargin<3;weightCriterion=5;end
if nargin<2;q=0.05;end


model=F.model;
N=F.parameters(1);
coef=abs(F.coef);
p=size(coef,2);
PredError=F.PredError;
variableIndex=1:p;
error005q=quantile(PredError,q);
index005=find(PredError<=error005q);
error005=PredError(index005);

coef005=coef(index005,:);
n005=length(index005);
Model005=nan(n005,p);
Z005=zeros(p,p);
maxErr=max(PredError);
for i=1:p
  temp=model{i};
  Model005(:,i)=ismember(index005,temp);  %+++ 0-1 valued...
end


for i=1:n005
  ki=find(Model005(i,:)==1);
  err=error005(i);
  cb=nchoosek(ki,2);
  maxDiffb=max(coef005(i,:))-min(coef005(i,:));
  for j=1:size(cb,1)
    a=cb(j,1);b=cb(j,2);
    diffb=abs((coef005(i,a)-coef005(i,b))); 
    score=(maxErr/(err+0.001))*cos(diffb*pi/2/maxDiffb)*(coef005(i,a)+coef005(i,b))/2;
    Z005(a,b)=Z005(a,b)+score;
    Z005(b,a)=Z005(b,a)+score;  
  end
end
%+++ output
Z=Z005+eps;
Z=Z/max(max(Z));



