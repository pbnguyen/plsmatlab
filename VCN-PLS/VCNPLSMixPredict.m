clear all;clf;
load('VCNMix.mat');

figure(1);
subplot(1,2,1);
bar(sum(G1));
title('Coca');
xlabel('variable index');ylabel('total complementary information');
subplot(1,2,2);
bar(sum(G2));
title('Pepsi');
xlabel('variable index');ylabel('total complementary information');

figure(2);
subplot(1,2,1);
imagesc(G1);
subplot(1,2,2);
imagesc(G2);

TempArr1 = sum(G1);
TempArr2 = sum(G2);
selectVarCoca = [];
selectVarPepsi = [];
for i=1:1000
    [value1,index1] =  max(TempArr1);
    selectVarCoca(i) = index1;
    TempArr1(index1) = [];
    
    [value,index2] =  max(TempArr2);
    selectVarPepsi(i) = index2;
    TempArr2(index2) = [];
  
end
selectVarCoca = sort(selectVarCoca);
selectVarPepsi = sort(selectVarPepsi);

XTrain = XTrainMix(:,selectVarCoca);
yTrain = yTrainMix(:,1);
method = 'center';
PLSMixResult1 = pls(XTrain,yTrain,A,method);
XTest  = XTestMix(:,selectVarCoca);
yTest  = yTestMix(:,1);
% y = WX
W1 = PLSMixResult1.regcoef_original;
X1 = [XTest ones(size(XTest,1),1)];
y1 = X1*W1;

XTrain = XTrainMix(:,selectVarPepsi);
XTest  = XTestMix(:,selectVarPepsi);
yTrain = yTrainMix(:,2);
PLSMixResult2 = pls(XTrain,yTrain,A,method);
W2 = PLSMixResult2.regcoef_original;
X2 = [XTest ones(size(XTest,1),1)];
y2 = X2*W2;

yTest  = yTestMix;
fprintf('\tValue \t\t\t\t Predict\r\n')
fprintf('Coca \t Pepsi \t\t Coca \t Pepsi\r\n');
RMSEP_Coca = 0;
RMSEP_Pepsi = 0;
for i=1:length(y1)
    RMSEP_Coca  =  RMSEP_Coca + (yTest(i,1)-y1(i))^2;
    RMSEP_Pepsi =  RMSEP_Pepsi + (yTest(i,2)-y2(i))^2;
    fprintf('%.2f \t %.2f \t\t %.2f \t %.2f \r\n',yTest(i,1),yTest(i,2),y1(i),y2(i));
end
figure(3);
subplot(1,2,1);
hold on; grid on;
syms x;
ezplot(x,[0 100]);
title('Coca');
xlabel('observed')
ylabel('predicted')
for i=1:length(y1)
    text(yTest(i,1),y1(i),'x','Color','red');
end
subplot(1,2,2);
hold on; grid on;
ezplot(x,[0 100]);
title('Pepsi');
xlabel('observed')
ylabel('predicted')
for i=1:length(y2)
    text(yTest(i,2),y2(i),'x','Color','red');
end
RMSEP_Coca  = sqrt(RMSEP_Coca/length(y1));
RMSEF_Coca = PLSMixResult1.RMSEF;
RMSEP_Pepsi  = sqrt(RMSEP_Pepsi/length(y1));
RMSEF_Pepsi = PLSMixResult2.RMSEF;
fprintf('Coca: RMSEF = %f, RMSEP = %f \r\n',RMSEF_Coca,RMSEP_Coca);
fprintf('Pepsi: RMSEF = %f, RMSEP = %f \r\n',RMSEF_Pepsi,RMSEP_Pepsi);

