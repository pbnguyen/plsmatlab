clear all;
load('..\data\Pepsi.mat');
XTrain = XTrainPepsi;
yTrain = yTrainPepsi;
XTest  = XTestPepsi;
yTest  = yTestPepsi;
A = 10;
method = 'center';
clf;
PLSPepsiResult = pls(XTrain,yTrain,A,method);
% y = WX
W = PLSPepsiResult.regcoef_original;
X = [XTest ones(size(XTest,1),1)];
y = X*W;
hold on; grid on;
syms x;
ezplot(x,[0 100]);
xlabel('observed')
ylabel('predicted')
fprintf('value \t\t predict \n');
RMSEP = 0;
for i=1:length(y)
    text(yTest(i),y(i),'x','Color','red')
    fprintf('%.2f \t\t %.2f \r\n',yTest(i),y(i));
    RMSEP = RMSEP + (yTest(i)-y(i))^2;
end
RMSEP = sqrt(RMSEP/length(y));
RMSEF = PLSPepsiResult.RMSEF;
fprintf('RMSEF = %f \r\n',RMSEF);
fprintf('RMSEP = %f \r\n',RMSEP);




