clear all;
load('..\data\all.mat');
SelectedVariables = TestIrivPepsi.SelectedVariables;
XTrain = XTrainPepsi(:,SelectedVariables);
yTrain = yTrainPepsi;
XTest  = XTestPepsi(:,SelectedVariables);
yTest  = yTestPepsi;
A = 14;
method = 'center';
clf;
PLSCocaResult = pls(XTrain,yTrain,A,method);

%for i=1:size(PLSCocaResult.X_pretreat,1)
%     plot(wavelength,PLSCocaResult.X_pretreat(i,:));
%end
title('Features Pretreat');
xlabel('Wavelength (nm)') 
ylabel('Intensity ratio')
% y = WX
W = PLSCocaResult.regcoef_original;
X = [XTest ones(size(XTest,1),1)];
y = X*W;
hold on; grid on;
syms x;
ezplot(x,[0 100]);
xlabel('observed')
ylabel('predicted')
fprintf('value \t\t predict \n');
RMSEP = 0;
for i=1:length(y)
    text(yTest(i),y(i),'x','Color','red')
    fprintf('%.2f \t\t %.2f \r\n',yTest(i),y(i));
    RMSEP = RMSEP + (yTest(i)-y(i))^2;
end
RMSEP = sqrt(RMSEP/length(y));
RMSEF = PLSCocaResult.RMSEF;
fprintf('RMSEF = %f \r\n',RMSEF);
fprintf('RMSEP = %f \r\n',RMSEP);

